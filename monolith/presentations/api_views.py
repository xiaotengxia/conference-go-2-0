from sys import exec_prefix
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods
from events.models import Conference
from events.api_views import ConferenceListEncoder
import json


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceListEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        try:
            count, _ = Presentation.objects.get(id=pk).delete()
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation id"}, status=400
            )
        return JsonResponse({"Delete": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
            if "status" in content:
                status = Status.objects.get(id=content["status"])
                content["status"] = status
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid conference id"})

    Presentation.objects.filter(id=pk).update(**content)
    presentation = Presentation.objects.get(id=pk)
    return JsonResponse(
        presentation, encoder=PresentationDetailEncoder, safe=False
    )
