from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
import random


def get_photo(city, state):
    # Use the Pexels API
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}"}
    res = requests.get(url, params=params, headers=headers)
    res = json.loads(res.content)
    try:
        index = random.randint(0, len(res["photos"]) - 1)
        return res["photos"][index]["url"]
    except len(res["photos"]) == 0:
        return None


def get_weather_data(city, state):
    # Use the Open Weather API
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city},{state}, USA", "appid": OPEN_WEATHER_API_KEY}
    res = requests.get(url, params=params)
    res = json.loads(res.content)
    lat = res[0]["lat"]
    lon = res[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    res = requests.get(url, params=params)
    res = json.loads(res.content)

    return {
        "description": res["weather"][0]["description"],
        "temperature": res["main"]["temp"],
    }
